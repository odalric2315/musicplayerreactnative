/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
TrackPlayer.setupPlayer({}).then(async () => {});
TrackPlayer.registerPlaybackService(() => require('./service.js').default);