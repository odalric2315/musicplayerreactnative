import React from 'react';
// import BottomNavigator from './BottomNavigator';
import AuthStackNavigator from './AuthStackNavigator';
import AuthProvider from './AuthContext';

function Providers(){
    return(
        <AuthProvider>
            <AuthStackNavigator/>
        </AuthProvider>
    )
}
export default Providers;
// export { BottomNavigator};
export {AuthStackNavigator};