import React, { createContext, useState } from 'react'
// import auth from '@react-native-firebase/auth';
import firebase from "@react-native-firebase/app";

export const AuthContext = createContext()

export default function AuthProvider({children}) {
    const [user, setUser] = useState(null);

    return (
        <AuthContext.Provider
          value={{
            user,
            setUser,
            login: async (email, password) => {
              try {
                await firebase.auth().signInWithEmailAndPassword(email, password);
              } catch (e) {
                console.log(e);
              }
            },
            register: async (email, password) => {
              try {
                await firebase.auth().createUserWithEmailAndPassword(email, password);
              } catch (e) {
                console.log(e);
              }
            },
            logout: async () => {
              try {
                await auth().signOut();
              } catch (e) {
                console.error(e);
              }
            },
          }}>
          {children}
        </AuthContext.Provider>
      );
    };