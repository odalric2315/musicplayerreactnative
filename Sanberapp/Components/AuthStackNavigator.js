import React from 'react';
import Login from '../pages/Login';
import Register from '../pages/Register'
import {createStackNavigator} from '@react-navigation/stack';

const AuthStack = createStackNavigator();

function AuthStackNavigator() {
  
  return (
    <AuthStack.Navigator initialRouteName={'Login'}>
      <AuthStack.Screen
        name="Login"
        component={Login}
        options={{header: () => null}}
      />
      <AuthStack.Screen
        name={'Register'}
        component={Register}
        options={{header: () => null}}
      />
    </AuthStack.Navigator>
  );
}

export default AuthStackNavigator;