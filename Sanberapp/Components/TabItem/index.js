import React from 'react'
import { StyleSheet, Text, TouchableOpacity } from 'react-native'
import {Homemusic, Profilemusic, Playlistmusic, Homemusicnonactive, Profilemusicnonactive, Playlistmusicnonactive} from '../../Asset'
import {warna_utama, warna_nonactive} from '../../Utils/constant'



const TabItem = ({isFocused, onPress, onLongPress, label}) => {
    const Icon = () => {
      if(label === 'Home') return isFocused ? <Homemusic/> : <Homemusicnonactive/>
      if(label === 'Playlist') return isFocused ? <Playlistmusic/> : <Playlistmusicnonactive/>
      if(label === 'Profile') return isFocused ? <Profilemusic/> : <Profilemusicnonactive/>
      return
      <Homemusicnonactive/>
    }
  return (
        <TouchableOpacity
            onPress={onPress}
            onLongPress={onLongPress}
            style={styles.container}
          >
            <Icon />
            <Text style={styles.text(isFocused)}>
              {label}
            </Text>
          </TouchableOpacity>
    )
}

export default TabItem

const styles = StyleSheet.create({
    container:{
      alignItems: 'center'
  },
  text:{
      fontSize: 13,
      color: isFocused ? warna_utama : warna_nonactive,
      marginTop: 8,
  }

})
