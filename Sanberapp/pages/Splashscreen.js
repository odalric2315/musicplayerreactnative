import React, { useEffect } from 'react'
import { StyleSheet, Text, View } from 'react-native'



const Splashscreen = ({navigation}) => {
    useEffect(() => {
        setTimeout(() =>{
            navigation.replace('Login');
        }, 3000)
    },[navigation]);
    return (
        
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Image source={require('../Asset/Logomusicblue.svg')} style={{width: 260, height: 260}}
        />
        <Text sytle={{fontSize: 36, fontWeight: "bold" ,marginTop: 30, color: white}}>
            Music Player
        </Text>
      </View>
    )
}

export default Splashscreen

const styles = StyleSheet.create({
    container: {
        margin: 30,
        flex: 1,
        alignSelf: "center",
        backgroundColor: 'black'
    }
})

