import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity, ActivityIndicator,
    Dimensions } from 'react-native'
import {Image} from 'react-native-elements';


const {width, height} = Dimensions.get('window');

const SongItem = (props) => {
    return (
      <View style={styles.songItem}>
        <View style={styles.songAndIcon}>
          <TouchableOpacity onPress={props.onSelect}>
            <View style={{flexDirection: 'row'}}>
              <Image
                source={{uri: props.artwork}}
                style={styles.songImg}
                PlaceholderContent={
                  <ActivityIndicator size="small" color="gray" />
                }
              />
              <View style={styles.titleAndartist}>
                <Text style={styles.songTitle}>{props.title}</Text>
                <Text style={styles.songArtist}>{props.artist}</Text>
              </View>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

export default SongItem

const styles = StyleSheet.create({songItem: {
    height: height / 12,
    padding: height / 75,
    borderBottomWidth: 1,
    borderBottomColor: '#222831',
    justifyContent: 'center',
  },
  songTitle: {
    fontSize: height / 41.6,
    color: 'white',
  },
  songAndIcon: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  songImg: {
    height: height / 18.75,
    width: height / 18.75,
  },
  titleAndartist: {
    paddingLeft: height / 75,
  },
  songArtist: {
    fontSize: height / 62,
    color: 'gray',
  }
});
