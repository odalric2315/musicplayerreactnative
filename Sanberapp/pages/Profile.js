import React from 'react'
import { View, StyleSheet, SafeAreaView, Text, Dimensions, Image, ScrollView, Button} from 'react-native';
import Constants from 'expo-constants';
import { Ionicons } from '@expo/vector-icons';
import { AuthContext } from "../Components/AuthContext";

export default function Profile() {
    const {logout} = useContext(AuthContext);

    return ( 
        <View style={styles.container}>
            <SafeAreaView>
                <ScrollView style={styles.scrollView}>
                    <Button onPress={() => logout()} title="LogOut"
                    buttonStyle={{
                    justifyContent: 'flex-end',
                    backgroundColor: "#7F7F7F",
                    padding: 15,
                    borderRadius: 40}}>
                    </Button>
                    <Text style={{top: 40, alignSelf: 'center', fontSize: 28, fontWeight: 'bold', color: '#FFFFFF'}}>
                        Tentang Saya
                    </Text>
                        <Image 
                            source={require('../Asset/odalric.jpg')} 
                            style={styles.logo}
                        />
                    <View style={{top: 100, alignSelf: 'center'}}>
                        <Text style={{alignSelf: 'center', fontSize: 20, fontWeight: 'bold', color: '#FFFFFF'}}>
                            Odalric
                        </Text>
                        <Text style={{color: '#3EC6FF', fontSize: 14, marginTop: 10, marginBottom: 20, alignSelf: 'center', fontWeight: 'bold'}}>
                            React Native Developer
                        </Text>
                    </View>
                        <View style={styles.square}>
                            <Text style={{margin: 10}}>Portofolio</Text>
                            <View style={{ borderBottomColor: 'black', borderBottomWidth: 1 }}/>
                            <Ionicons style={{ alignSelf: 'center', paddingTop: 10, justifyContent: 'center'}} name="logo-github" size={72} color="#003366"/>
                            <Text style={{alignSelf: 'center', fontWeight: 'bold', justifyContent: 'center'}}>@Odalric2315</Text>
                        </View>
                    <View style={{margin: 10}}/>
                        <View style={styles.square}>
                            <Text style={{margin: 10}}>Hubungi Saya</Text>
                            <View style={{ borderBottomColor: 'black', borderBottomWidth: 1 }}/>
                            <Ionicons style={{ alignSelf: 'center', paddingTop: 20 }} name="logo-instagram" size={72} color="#3f729b"/>
                            <Text style={{alignSelf: 'center', fontWeight: 'bold'}}>Dalic17</Text>
                            <Ionicons style={{ alignSelf: 'center', paddingTop: 20 }} name="logo-whatsapp" size={70} color="black" />
                            <Text style={{alignSelf: 'center', fontWeight: 'bold'}}>087894211839</Text>
                        </View>
                    </ScrollView> 
            </SafeAreaView>
        </View>
        
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#222831',
        marginTop: Constants.statusBarHeight,
    },
    text: {
        margin: 10,
        marginLeft: 40
    },
    scrollView: {
        backgroundColor: 'white',
      },
    logo:{
        borderRadius: 100, 
        width: 150, 
        height: 150, 
        top: 70, 
        marginLeft: 20, 
        marginRight: 10, 
        backgroundColor: 'lightgrey', 
        alignSelf: 'center'
    },
    square: {
        top: 100,
        height: 180,
        width: Dimensions.get('screen').width - 40,
        alignSelf: 'center',
        borderRadius: 20,
        backgroundColor: 'lightgrey',
    },
})
