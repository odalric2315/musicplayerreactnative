import React, {useState} from 'react'
import { StyleSheet, Text, View, Dimensions } from 'react-native'
import {SearchBar} from 'react-native-elements';
import { ScrollView } from 'react-native-gesture-handler'
import songs from '../Asset/Songs/datasongs'
import SongItem from './SongItem';

const {width, height} = Dimensions.get('window');

const Playlist = (props) => {
  const [search, setSearch] = useState('');
  const [data, setData] = useState([]);
  const arrayHolder = [
      ...songs
  ];

  const searchFilterFunction = (text) => {
    const newData = arrayHolder.filter((item) => {
      const itemData = item.title ? item.title.toUpperCase() : ''.toUpperCase();
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });
    setSearch(text);
    setData(newData);
  };
  
    return (
        <View style={{flex: 1, padding: height / 37, backgroundColor: '#222831'}}>
          <SearchBar
            round={true}
            containerStyle={{backgroundColor: 'black'}}
            inputContainerStyle={{backgroundColor: '7F7F7F', padding: 5}}
            inputStyle={{color: 'white'}}
            placeholderTextColor="gray"
            placeholder="Search for songs..."
            value={search}
            underlineColorAndroid={{color: '#e36b02'}}
            onChangeText={(text) => {
              searchFilterFunction(text);
            }}
            onClear={() => searchFilterFunction('')}
            searchIcon={{color: '#e36b02'}}
          />
    
          <ScrollView style={{marginTop: height/37}}>
            <View>
              {data.map((item) => (
                <SongItem
                  artwork={item.artwork}
                  title={item.title}
                  artist={item.artist}
                  onSelect={() =>
                    props.navigation.navigate('MusicPlayer', {
                      sid: item.id,
                    })
                  }
                />
              ))}
            </View>
          </ScrollView>
        </View>
      );
  };

export default Playlist

const styles = StyleSheet.create({})
