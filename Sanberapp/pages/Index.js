import MusicPlayer from "./MusicPlayer";
import Login from "./Login";
import Register from "./Register";
import Splashscreen from "./Splashscreen";
import Playlist from "./Playlist";
import Profile from "./Profile";

export { MusicPlayer, Login, Register, Splashscreen, Playlist,Profile}