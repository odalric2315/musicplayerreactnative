import React from "react";
import { StyleSheet,Text, View, Image, StatusBar } from "react-native";
import Icon from "react-native-vector-icons/Ionicons";
import { Input, Button } from "react-native-elements";
import songs from '../Asset/Songs/datasongs'
import firebase from "@react-native-firebase/app";
import { AuthContext } from "../Components/AuthContext";

const Register = props => {
  const navigation = props.navigation;
  const {register} = useContext(AuthContext);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const submit = () => {
    const songs = {
      email,
      password,
    }
  
  firebase
  .auth()
  .createUserWithEmailAndPassword(email, password)
  .then((userCredential) => {
    // Signed in
    var user = userCredential.user;
    console.log(user)
    alert('Registration Success')
    // ...
  })
  .catch((error) => {
    var errorCode = error.code;
    var errorMessage = error.message;
    Alert.alert('Registration Failed', errorMessage)
    // ..
  });
  }
  return (
    <View style={styles.container}>
      <View
        style={{
          flex: 1,
          paddingTop: 40,
          backgroundColor: "#fff",
          alignItems: "center"
        }}
      >
        <Button
          icon={<Icon name="arrow-back-outline" size={25} color="#333" />}
          buttonStyle={{
            backgroundColor: "#fff"
          }}
          containerStyle={{
            position: "absolute",
            zIndex: 9,
            top: 10,
            left: 10
          }}
          onPress={() => navigation.navigate("Login")}
        />
        <Image
          source={require('../Asset/Logomusicblue.svg')}
          resizeMode="contain"
          style={{
            height: "90%",
            width: "90%"
          }}
        />
      </View>

      <View
        style={{
          flex: 2,
          backgroundColor: "#fff",
          alignItems: "center",
          paddingHorizontal: 20
        }}
      >
        <Text
          style={{
            fontSize: 20,
            color: "#636262",
            padding: 20,
            marginBottom: 10
          }}
        >
          Create New Account
        </Text>
        <View>
          <Input
            inputContainerStyle={{
              borderColor: "#ddd",
              borderWidth: 1,
              width: "100%",
              paddingHorizontal: 10,
              paddingTop: 1,
              paddingBottom: 3,
              borderRadius: 5
            }}
            inputStyle={{
              fontSize: 14
            }}
            placeholder="Name"
            leftIcon={<Icon name="person-sharp" size={18} color="#cacaca" />}
          />
          <Input
            inputContainerStyle={{
              borderColor: "#ddd",
              borderWidth: 1,
              width: "100%",
              paddingHorizontal: 10,
              paddingTop: 1,
              paddingBottom: 3,
              borderRadius: 5
            }}
            inputStyle={{
              fontSize: 14
            }}
            maxLength={30}
            placeholder="Email"
            leftIcon={<Icon name="mail-outline" size={18} color="#cacaca" />}
          />
          <Input
            inputContainerStyle={{
              borderColor: "#ddd",
              borderWidth: 1,
              width: "100%",
              paddingHorizontal: 10,
              paddingTop: 1,
              paddingBottom: 3,
              borderRadius: 10
            }}
            inputStyle={{
              fontSize: 14
            }}
            maxLength={20}
            placeholder="Password"
            secureTextEntry={true}
            leftIcon={<Icon name="lock-closed" size={18} color="#cacaca" />}
          />
          <View
            style={{
              paddingHorizontal: 10
            }}
          >
            <Button
              onPress={submit}
              title="REGISTER"
              buttonStyle={{
                backgroundColor: "#8462f5",
                padding: 12,
                width: "100%",
                borderRadius: 40
              }}
            />
          </View>

          <View
            style={{
              flexDirection: "row",
              marginVertical: 10,
              paddingHorizontal: 20
            }}
          >
            <Text
              style={{
                color: "#bfbfbf"
              }}
            >
              Already have an account ?
            </Text>
            <Text onPress={() => navigation.navigate('Login')}>
            <Text isPress
              style={{
                color: "#8462f5"}}>
              {" "}
              Login here
            </Text>
            </Text>
          </View>
        </View>
      </View>
      <StatusBar
        barStyle="dark-content"
        backgroundColor="#fff"
        translucent={true}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    marginTop: StatusBar.currentHeight
  }
});
export default Register;