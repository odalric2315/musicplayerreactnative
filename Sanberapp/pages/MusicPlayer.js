import React, {useEffect, useRef, useState} from 'react'
import { StyleSheet, Text, View, SafeAreaView, Dimensions,Image,Animated,TouchableOpacity,FlatList} from 'react-native'
import Slider from '@react-native-community/slider';
import Ionicons from 'react-native-vector-icons/Ionicons';
import TrackPlayer, {
    Capability,
    Event,
    State,
    usePlaybackState,
    useProgress,
    useTrackPlayerEvents
} from 'react-native-track-player';
import songs from '../Asset/Songs/datasongs';
import { AuthContext } from "../Components/AuthContext";

const { width, height} = Dimensions.get('window');
const {logout} = useContext(AuthContext);

const setupPlayer = async() => {
    await TrackPlayer.setupPlayer();
    await TrackPlayer.updateOptions({
        capabilities: [
            Capability.Play,
            Capability.Pause,
            Capability.SkipToNext,
            Capability.SkipToPrevious,
            Capability.Stop,
        ],
        stopWithApp: true
    })
    await TrackPlayer.add(songs);

    await TrackPlayer.play();
}

const tooglePlayback = async(playbackState) => {
    const currentTrack = await TrackPlayer.getCurrentTrack();
if(currentTrack !== null){
    if( playbackState === State.Paused){
        await TrackPlayer.play();
    } else {
        await TrackPlayer.pause();
    }
}

}

const MusicPlayer = () => {
    const playbackState = usePlaybackState();
    const progress = useProgress();

    const [trackArtwork, setTrackArtwork] = useState();
    const [trackArtist, setTrackArtist] = useState();
    const [trackTitle, setTrackTitle] = useState();
    
    const scrollX = useRef(new Animated.Value(0)).current;
    const [songIndex, setSongIndex] = useState(0);

    const songSlider = useRef(null);

    useTrackPlayerEvents([Event.PlaybackTrackChanged], async event =>{
        if( event.type === Event.PlaybackTrackChanged && event.nextTrack !== null){
            const track = await TrackPlayer.getTrack(event.nextTrack);
            const {title, artwork, artist} = track;
            setTrackTitle(title);
            setTrackArtwork(artwork);
            setTrackArtist(artist);
        }
    })

    const skipTo = async(trackId) => {
        await TrackPlayer.skip(trackId);
    }

useEffect(() => {
    setupPlayer();
    scrollX.addListener(({Value}) => {
        const index = Math.round(value / width);
        skipTo(index); 
        setSongIndex(index);
    })

    return () => {
        scrollX.removeAllListeners();
    }
}, []);

    const skipToNext = () => {
        songSlider.current.scrollToOffSet({
            offset: (songIndex + 1) * width,
        });
    };

    const skipToPrevious = () => {
        songSlider.current.scrollToOffSet({
            offset: (songIndex - 1) * width,
        });
    };

    const renderSongs = ({index, item}) => {
    return (
        <Animated.View style={{width: width,
        justifyContent: 'center',
        alignItems: 'center'
        }}>
            <View style={styles.artworkWrapper}>
            <Image source={trackArtwork}
            style={styles.artworkImg}
            />
            </View>
        </Animated.View>
    )
}
    return (
        <SafeAreaView style={styles.container}>
            <Button onPress={() => logout()} title="LogOut"
              buttonStyle={{
                justifyContent: 'flex-end',
                backgroundColor: "#7F7F7F",
                padding: 15,
                borderRadius: 40}}>
                    </Button>
        <View style={styles.mainContainer}>
            <View style={{width:width}}>
            <Animated.FlatList
                ref={songSlider}
                data={songs}
                renderItem={renderSongs}
                keyExtractor={(item) => item.id}
                horizontal
                pagingEnabled
                showsHorizontalScrollIndicator={false}
                scrollEventThrottle={16}
                onScroll={Animated.event(
                    [{nativeEvent: {
                        contentOffset: {x: scrollX}
                    }}],
                    {useNativeDriver: true}
                )}
                />
            </View>
            <View>
                <Text style={styles.title}>{trackTitle}</Text>
                <Text style={styles.artist}>{trackArtist}</Text>
            </View>

            <View>
                <Slider
                style={styles.progressContainer}
                value={progress.position}
                minimumValue={0}
                maximumValue={progress.duration}
                thumbTintColor='#FFD369'
                minimumTrackTintColor='FFD369'
                maximumTrackTintColor='#FFF'
                onSlidingComplete={ async (value) => {
                    await TrackPlayer.seekTo(value)
                }}
                />
            <View style={styles.progressLabelContainer}>
                <Text style={styles.progressLabelText}>
                    { new Date(progress.position * 1000).toISOString().substr(14, 5)} 
                </Text>
                <Text style={styles.progressLabelText}>
                { new Date((progress.duration - progress.position) * 1000).toISOString().substr(14, 5)} 
                </Text>
            </View>
            
            <View style={styles.musicControl}>
                <TouchableOpacity onPress={skipToPrevious}>
                    <Ionicons name='play-skip-back-outline' size={35} color='FFD369' style={{marginTop:25}}/>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => tooglePlayback(playbackState)}>
                    <Ionicons name={playbackState == State.Playing ? 'ios-pause-circle' : 'ios-play-circle'} size={75} color='FFD369'/>
                </TouchableOpacity>
                <TouchableOpacity onPress={skipToNext}>
                    <Ionicons name='play-skip-forward-outline' size={35} color='FFD369'style={{marginTop:25}}/>
                </TouchableOpacity>
            </View>
        </View>
        </View>
        </SafeAreaView>
    )






}
export default MusicPlayer

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: '#222831'
    },
    mainContainer:{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    artworkWrapper:{
        width: 300,
        height: 350,
        marginBottom: 25,
        elevation: 5
    },
    artworkImg:{
        width: '100%',
        height: '100%',
        borderRadius: 15
    },
    title:{
        fontSize: 18,
        fontWeight: '600',
        textAlign: 'center',
        color: '#EEEEEE'
    },
    artist:{
        fontSize: 15,
        fontWeight: '250',
        textAlign: 'center',
        color: '#EEEEEE'
    },
    progressContainer:{
        width: 350,
        height: 40,
        marginTop: 30,
        flexDirection: 'row'
    },
    progressLabelContainer:{
        width: 340,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    progressLabelText:{
        color: '#FFF'
    },
    musicControl:{
        flexDirection: 'row',
        width: '60%',
        justifyContent: 'space-between',
        marginTop: 20
    },
    bottomContainer:{

    },
    bottomControl:{

    },

})
