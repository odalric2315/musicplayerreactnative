const songs = [
    {
        title: 'Unstoppable',
        artist: 'Sia',
        artwork: require('../../Asset/sia-unstoppable.jpg'),
        url: require('../Songs/Sia-unstoppable.mp3'),
        id: 1,
        duration:337,
    },
    {
        title: 'Know Me Too Well',
        artist: 'New Hope Club',
        artwork: require('../../Asset/knowmetoowell.jpg'),
        url: require('../Songs/New Hope Club-knowmetoowell.mp3'),
        id: 2,
    },
    {
        title: 'Here is Your Perfect',
        artist: 'Jamie Miller',
        artwork: require('../../Asset/hereyourperfect.jpg'),
        url: require('../Songs/Jamie Miller-hereisyourperfect.mp3'),
        id: 3,
    },
    {
        title: 'It is My Life',
        artist: 'Bon Jovi',
        artwork: require('../../Asset/bonjovi.jpg'),
        url: require('../Songs/Bon Jovi -itismylife.mp3'),
        id: 4,
    },
    {
        title: 'Lily',
        artist: 'Alan Walker',
        artwork: require('../../Asset/lili.jpg'),
        url: require('../Songs/Alan Walker-lily.mp3'),
        id: 5,
    },
    {
        title: 'White Flag',
        artist: 'Dido',
        artwork: require('../../Asset/dido.jpg'),
        url: require('../Songs/Dido-whiteflag.mp3'),
        id: 6,
    },
    {
        title: 'I Will Survive',
        artist: 'Gloria Gaynor',
        artwork: require('../../Asset/gloria.jpg'),
        url: require('../Songs/Gloria Gaynor-iwillsurvive.mp3'),
        id: 7,
    },
    {
        title: 'Beautiful in White',
        artist: 'Shane Filan',
        artwork: require('../../Asset/shane.jpg'),
        url: require('../Songs/Shane Filan-beautifulinwhite.mp3'),
        id: 8,
    }
];
export default songs