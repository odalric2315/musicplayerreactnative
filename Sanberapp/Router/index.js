import React, { Profiler } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { MusicPlayer, Login, Register, Playlist, Profile, Splashscreen}from '../pages/Index';
import {BottomNavigator} from '../Components/BottomNavigator/Index'
import Providers from '../Components';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const MainApp = () => {
    return (
      <Tab.Navigator tabBar={props => <BottomNavigator {...props}/>} >
        <Tab.Screen name="MusicPlayer" component={MusicPlayer} />
        <Tab.Screen name="Playlist" component={Playlist} />
        <Tab.Screen name="Profile" component={Profile} />
      </Tab.Navigator>
    )
}

const Router = () => {
    return (
    <Providers>
      <Stack.Navigator initialRouterName="Splash">
      <Stack.Screen name="Splash" component={Splashscreen} options={{headerShown: false}}/>
      <Stack.Screen name="Login" component={Login} options={{headerShown: false}}/>
      <Stack.Screen name="Register" component={Register} options={{headerShown: false}}/>
      <Stack.Screen name="MainApp" component={MainApp} options={{headerShown: false}}/>
      </Stack.Navigator>
    </Providers>
    )
}

export default Router;

const styles = StyleSheet.create({})
