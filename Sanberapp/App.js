import StatusBar from 'react-native';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { NavigationContainer } from "@react-navigation/native";
import Router from './Router/index';
import firebase from "@react-native-firebase/app";

const firebaseConfig = {
  apiKey: "AIzaSyAl-XXr6E8JT-kn4brZiqOzH36-ZQ8g5G4",
  authDomain: "sanbercodeapp-797b6.firebaseapp.com",
  projectId: "sanbercodeapp-797b6",
  storageBucket: "sanbercodeapp-797b6.appspot.com",
  messagingSenderId: "143977205494",
  appId: "1:143977205494:web:a5b814ad6b168639baa6d7"
};

if(firebase.apps.length){
initializeApp(firebaseConfig);
}


export default function App() {
  return (
    <NavigationContainer>
      <StatusBar barStyle= 'light-content'/>
      <Router />
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
